
import os
import sys
import random
import shutil
import unittest
import tempfile

from contextlib import closing
from distutils import ccompiler
from ctypes import cast, c_int, py_object, CFUNCTYPE, PYFUNCTYPE

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen


def download_SampleDLL(temp):
    SAMPLEDLL = {
        "SampleDLL.cpp": (
            "https://github.com/fancycode/MemoryModule/"
            "raw/master/example/SampleDLL/SampleDLL.cpp"
        ),
        "SampleDLL.h": (
            "https://github.com/fancycode/MemoryModule/"
            "raw/master/example/SampleDLL/SampleDLL.h"
        ),
    }
    for fname, url in SAMPLEDLL.items():
        with closing(urlopen(url)) as res:
            with open(os.path.join(temp, fname), "wb") as fp:
                fp.write(res.read())


def build_SampleDLL(temp):
    sources = filter(lambda f: f.endswith(".cpp"), os.listdir(temp))
    compiler = ccompiler.new_compiler()
    objects = compiler.compile(
        sources=[os.path.join(temp, f) for f in sources],
        output_dir=temp,
        macros=[("SAMPLEDLL_EXPORTS", None)],
        include_dirs=[temp],
    )
    compiler.link_shared_object(
        objects=objects,
        output_filename="SampleDLL.dll",
        output_dir=temp,
        library_dirs=[temp],
    )


class PyMemoryModuleTest(unittest.TestCase):
    def setUp(self):
        self.temp = tempfile.mkdtemp()

    def tearDown(self):
        shutil.rmtree(self.temp)

    def test_structure(self):
        mod = __import__("pymemorymodule")
        funcs = [
            "MemoryLoadLibrary",
            "MemoryGetProcAddress",
            "MemoryFreeLibrary",
        ]
        for func in funcs:
            self.assertTrue(
                hasattr(mod, func),
                "no %r attribute" % func
            )
            self.assertTrue(
                callable(getattr(mod, func)),
                "%r is not callable" % getattr(mod, func)
            )

    def test_illegal_arguments(self):
        mod = __import__("pymemorymodule")
        cases = [
            (mod.MemoryLoadLibrary, ()),
            (mod.MemoryLoadLibrary, (None, )),
            (mod.MemoryLoadLibrary, ("", )),
            (mod.MemoryLoadLibrary, ("".encode("ascii"), )),
            (mod.MemoryLoadLibrary, (1, 2, 3, )),
            (mod.MemoryGetProcAddress, ()),
            (mod.MemoryGetProcAddress, (1, "")),
            (mod.MemoryGetProcAddress, (None, None)),
            (mod.MemoryFreeLibrary, ()),
            (mod.MemoryFreeLibrary, (None, )),
            (mod.MemoryFreeLibrary, (1, )),
            (mod.MemoryFreeLibrary, (1, 2, 3)),
        ]
        for func, args in cases:
            try:
                ret = func(*args)
                self.fail("%r with %r returns %r", func, args, ret)
            except (TypeError, OSError):
                pass

    def test_functions_with_SampleDLL(self):
        mod = __import__("pymemorymodule")

        download_SampleDLL(self.temp)
        build_SampleDLL(self.temp)

        with open(os.path.join(self.temp, "SampleDLL.dll"), "rb") as fp:
            handle = mod.MemoryLoadLibrary(fp.read())
            addNumbers = cast(
                mod.MemoryGetProcAddress(handle, "addNumbers"),
                CFUNCTYPE(c_int, c_int, c_int)
            )
            x, y = random.sample(range(1000), 2)
            self.assertEqual(
                addNumbers(x, y), x + y,
                "loaded DLL doesn't work well"
            )
            try:
                ret = mod.MemoryGetProcAddress(handle, "XYZ")
                self.fail("MemoryGetProcAddress with 'XYZ' returns %r" % ret)
            except OSError:
                pass
            mod.MemoryFreeLibrary(handle)

    def test_functions_with__ctypes_pyd(self):
        mod = __import__("pymemorymodule")

        if sys.version_info[0] < 3:
            initfuncname = "init_ctypes"
            initfunctype = PYFUNCTYPE(None)
        else:
            initfuncname = "PyInit__ctypes"
            initfunctype = PYFUNCTYPE(py_object)

        with open(__import__("_ctypes").__file__, "rb") as fp:
            handle = mod.MemoryLoadLibrary(fp.read())
            initfunc = cast(
                mod.MemoryGetProcAddress(handle, initfuncname),
                initfunctype
            )

            if sys.version_info[0] < 3:
                sys.modules.pop("_ctypes", None)
                initfunc()
                _ctypes = sys.modules["_ctypes"]
            else:
                _ctypes = initfunc()

            self.assertEqual(_ctypes.__name__, "_ctypes", repr(_ctypes))
            self.assertFalse(hasattr(_ctypes, "__file__"), repr(_ctypes))
            self.assertTrue("LoadLibrary" in dir(_ctypes), repr(_ctypes))
            self.assertTrue("FreeLibrary" in dir(_ctypes), repr(_ctypes))


def main():
    try:
        from xmlrunner import XMLTestRunner
        with open("results.xml", 'wb') as fp:
            unittest.main(testRunner=XMLTestRunner(output=fp))
    except ImportError:
        unittest.main()


if __name__ == "__main__":
    main()
